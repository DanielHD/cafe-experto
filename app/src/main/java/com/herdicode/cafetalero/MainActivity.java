package com.herdicode.cafetalero;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Button btnCoffeCaracteristic;
    //ArrayAdapter para conectar el Spinner a nuestros recursos strings.xml
    protected ArrayAdapter<CharSequence> adapter;
    int position;
    String coffeElement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnCoffeCaracteristic = (Button) findViewById(R.id.btn_coffee_characteristics);

        Bundle params = new Bundle();


        Spinner spinner = (Spinner) findViewById(R.id.coffe_spinner);

        //Asignas el origen de datos desde los recursos
        adapter = ArrayAdapter.createFromResource(this, R.array.coffees,
                android.R.layout.simple_spinner_dropdown_item);

        //Asignas el layout a inflar para cada elemento
        //al momento de desplegar la lista
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Seteas el adaptador
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);

        btnCoffeCaracteristic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position == 0){
                    Toast.makeText(getApplicationContext(), "Por favor seleccione un cafe", Toast.LENGTH_SHORT).show();
                }else{
                    params.putString("coffeElement", coffeElement);
                    Intent intent = new Intent(getApplicationContext(), ScientificDescriptionActivity.class);
                    intent.putExtra("params", params);
                    startActivity(intent);
                }
            }
        });
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        this.position = position;
        this.coffeElement = (String) parent.getItemAtPosition(position);
        //Toast.makeText(getApplicationContext(), "Seleccionado "+coffeElement, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}