package com.herdicode.cafetalero;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CoffeExpert {

    String name;
    String description;

    public CoffeExpert(String name, String description) {
        this.name = name;
        this.description = description;
    }
    public static final List<CoffeExpert> COFFEES = new ArrayList<CoffeExpert>();
    static {
        COFFEES.add(new CoffeExpert("Arábica (Coffea Arábica)", "La variedad de café Arábica es originaria de Etiopía, se caracteriza desde un punto de vista aromático por un sabor dulce y afrutado, en gran parte porque su grano tiene un bajo contenido en cafeína, que se estima entre un 1,5% y 1,7%. En algunos foros está catalogado como un Gourmet Coffee."));
        COFFEES.add(new CoffeExpert("Robusta (Coffea Canephora)", "La variedad de café Robusta es originaria del África Central, ya que crece en ecosistemas secos, lo que influye en su característico sabor amargo y con mucho cuerpo. Su sabor suele tener matices de frutos secos y madera."));
        COFFEES.add(new CoffeExpert("Libérica (Coffea Libérica)", "Esta variedad es originaria de Monrovia (Liberia), de donde toma su nombre. El aroma y sabor de este grano de café es muy peculiar, por lo que su consumo está muy poco extendido, se centra principalmente en los países Escandinavos. "));
        COFFEES.add(new CoffeExpert("Excelsa (Coffea Excelsa)", "Su origen está fechado sobre 1905 en la región semiárida  del Lago Chad (África). Guarda grandes similitudes con el café Liberiano, en lo que respecta al tamaño del árbol y de sus hojas. Sin embargo, tanto sus flores como sus frutos y hojas tienen un tamaño sensiblemente menor."));
    }
    static CoffeExpert searchCoffe(String searchString) {
        CoffeExpert coffeExpert = null;
        for(CoffeExpert coffe: COFFEES){
            if(coffe.getName().equals(searchString)){
                coffeExpert = coffe;
            }
        }
        return coffeExpert;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
