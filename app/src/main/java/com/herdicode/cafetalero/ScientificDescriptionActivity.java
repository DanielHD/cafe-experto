package com.herdicode.cafetalero;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import static com.herdicode.cafetalero.CoffeExpert.searchCoffe;

public class ScientificDescriptionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scientific_description);
        TextView tv_coffe_name = (TextView) findViewById(R.id.tv_coffee_name);
        TextView tv_coffe_description = (TextView) findViewById(R.id.tv_coffee_description);
        Bundle params = getIntent().getExtras().getBundle("params");
        if (params != null) {
            String coffeElement = params.getString("coffeElement");
            //Toast.makeText(getApplicationContext(), "coffeElement a buscar: "+coffeElement, Toast.LENGTH_SHORT).show();

            CoffeExpert coffeExpert = searchCoffe(coffeElement);
            tv_coffe_name.setText(coffeExpert.getName());
            tv_coffe_description.setText(coffeExpert.getDescription());
            //Toast.makeText(getApplicationContext(), "Descripción encontradop: "+coffeExpert.getDescription(), Toast.LENGTH_SHORT).show();

        }
    }
}